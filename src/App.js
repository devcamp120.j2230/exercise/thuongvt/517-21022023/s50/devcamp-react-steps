
import './App.css';
import Steps from './component/steps';

const steps = [
  {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
  {id: 2, title: 'Installation', content: 'You can install React from npm.'},
  {id: 3, title: 'Create react app', content: 'Run create-react-app to run project.'},
  {id: 4, title: 'Run init project', content: 'Cd into project and npm start to run project.'},
];


function App() {
  return (
    <div >
      <ul>
        {steps.map((value,index)=>{  // duyệt qua từng mảng steps lấy id title content truyền dạng props cho component Steps
          return <li key={index}>
            <Steps id={value.id} title={value.title} content={value.content}></Steps>
          </li>
        })}
      </ul>
    </div>
  );
}

export default App;
